import Vue from 'vue';
import Router from 'vue-router';
import Login from '../components/Login.vue'
import Register from '../components/Register.vue'
import Home from '../components/Home';
import { store } from './../store'

Vue.use(Router);

export const router = new Router({
  mode: 'history',
  routes: [
    { path: '/login', component: Login },
    { path: '/register', component: Register },
    { path: '/home', component: Home },
    { path: '*', redirect: '/login' }
  ]
});

router.beforeEach((to, from, next) => {
  // redirect to login page if not logged in and trying to access a restricted page
  const publicPages = ['/login', '/register'];
  const authRequired = !publicPages.includes(to.path);

  if (authRequired && !store.state.user.userLoggued) {
    return next('/login');
  }

  next();
})
