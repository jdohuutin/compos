import firebase from 'firebase/app'
import 'firebase/firestore'
import 'firebase/auth';

const config = {
    apiKey: process.env.VUE_APP_APIKEY,
    projectId: process.env.VUE_APP_PROJECT_ID,
};

const db = firebase
    .initializeApp(config)
    .firestore()

export {
    firebase,
    db,
}
