import Vue from 'vue';
import Vuex from 'vuex';

import { player } from './player';
import { user } from './user';

Vue.use(Vuex);

export const store = new Vuex.Store({
    modules: {
        player,
        user
    }
});
