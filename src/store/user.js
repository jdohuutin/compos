// import { userService } from '../_services';
import { router } from '../helpers/router';
import {firebase} from '../helpers/firebase';

const userLoggued = JSON.parse(localStorage.getItem('user'));

const state = {
    userLoggued: userLoggued,
    statusRegister: null,
    errorRegister: null,
};

const mutations = {
    registerSuccess(state, user) {
        state.userLoggued = user;
    },
    setErrorRegister(state, error) {
        state.errorRegister = error
    },
    setStatusRegister(state, status) {
        state.statusRegister = status
    }
};

const actions = {
    login({ commit }, { email, password }) {
        firebase.auth().signInWithEmailAndPassword(email, password)
            .then(() => {
                commit('registerSuccess', user);
                localStorage.setItem('user', JSON.stringify(user));
                router.push('/home');
            })
            .catch((error) => {
                commit('setErrorRegister', error.message)
                commit('setStatusRegister', 'error')
            })
    },
    register({ commit }, payload) {
        firebase.auth().createUserWithEmailAndPassword(payload.email, payload.password)
            .then(() => {
                let user = firebase.auth().currentUser;

                user.updateProfile({
                    displayName: payload.pseudo,
                }).then(function() {
                    commit('registerSuccess', user);
                    localStorage.setItem('user', JSON.stringify(user));
                    router.push('/home');
                }).catch(function(error) {
                    commit('setErrorRegister', error.message)
                    commit('setStatusRegister', 'error')
                });
            })
            .catch((error) => {
                commit('setErrorRegister', error.message)
                commit('setStatusRegister', 'error')
            })
    }
};

export const user = {
    namespaced: true,
    state,
    actions,
    mutations
};
