import {db} from '../helpers/firebase';

const computeStats = function (teamates) {
    let starters = teamates.filter((player) => player !== null)

    if (starters.length === 0) {
        return null
    }

    const computeAttribute = function (starters, stat) {
        return Math.floor(starters.map(player => player.stats[stat]).reduce((sum, stat) => sum + stat, 0) / starters.length)
    }

    return {
        attack: computeAttribute(starters, 'attack'),
        defense: computeAttribute(starters, 'defense'),
        stamina: computeAttribute(starters, 'stamina'),
        speed: computeAttribute(starters, 'speed'),
    }
}

const state = {
    players: [],
    playersCount: 5,
    reds: new Array(5),
    blues: new Array(5),
    displayPlayerModal: false,
    selectPlayer: null
}

const mutations = {
    changePlayersCount(state, playersCount) {
        state.playersCount = playersCount

        while (state.reds.length < playersCount) {
            state.reds.push(null)
        }
        state.reds.splice(playersCount)

        state.blues.splice(playersCount)
        while (state.blues.length < playersCount) {
            state.blues.push(null)
        }
    },
    pickRedPlayer(state, player) {
        for (let i = 0; i < state.reds.length; i++) {
            if (!state.reds[i]) {
                state.reds.splice(i, 1, player)
                player.picked = true
                break
            }
        }
    },
    pickBluePlayer(state, player) {
        for (let i = 0; i < state.blues.length; i++) {
            if (!state.blues[i]) {
                state.blues.splice(i, 1, player)
                player.picked = true
                break
            }
        }
    },
    unpickRedPlayer(state, player) {
        var index = state.reds.indexOf(player)
        if (index !== -1) {
            state.reds.splice(index, 1, null)
            player.picked = false
        }
    },
    unpickBluePlayer(state, player) {
        var index = state.blues.indexOf(player)
        if (index !== -1) {
            state.blues.splice(index, 1, null)
            player.picked = false
        }
    },
    fetchPlayers(state, players) {
        state.players = players
    },
    resetSelectPlayer(state) {
        state.selectPlayer = null;
    },
    updateSelectPlayer(state, player) {
        state.selectPlayer = player;
    },
    enabledModalPlayer(state) {
        state.displayPlayerModal = true;
    },
    disabledModalPlayer(state) {
        state.displayPlayerModal = false;
    }
}

const actions = {
    changePlayersCount({commit}, playersCount) {
        commit('changePlayersCount', playersCount)
    },
    pickRedPlayer({commit}, player) {
        commit('pickRedPlayer', player)
    },
    pickBluePlayer({commit}, player) {
        commit('pickBluePlayer', player)
    },
    unpickRedPlayer({commit}, player) {
        commit('unpickRedPlayer', player)
    },
    unpickBluePlayer({commit}, player) {
        commit('unpickBluePlayer', player)
    },
    fetchPlayers({commit}) {
        let players = []
        db.collection("players").get().then((querySnapshot) => {
            querySnapshot.forEach((doc) => {
                let player = doc.data();
                player.id = doc.id;
                players.push(player)
            });
        });

        commit('fetchPlayers', players)
    },
    addNewPlayer({commit, dispatch}, player) {
        db.collection('players').add(player).then(() => {
            commit('disabledModalPlayer');
            dispatch('fetchPlayers');
        });
    },
    updatePlayer({state, commit}, player) {
        db.collection('players')
            .doc(state.selectPlayer.id)
            .set(player)
            .then(() => {
                let indexUpdate = state.players.findIndex(player => player.id === state.selectPlayer.id);
                player.id = state.selectPlayer.id;
                state.players.splice(indexUpdate, 1, player);
                commit('disabledModalPlayer');
                commit('resetSelectPlayer');
            })
    },
    deletePlayer({commit}) {
        db.collection('players')
            .doc(state.selectPlayer.id)
            .delete()
            .then(() => {
                let indexDelete = state.players.findIndex(player => player.id === state.selectPlayer.id);
                state.players.splice(indexDelete, 1);
                commit('disabledModalPlayer');
                commit('resetSelectPlayer');
            })
    }
}

const getters = {
    redsStats: state => {
        return computeStats(state.reds)
    },
    bluesStats: state => {
        return computeStats(state.blues)
    },
    displayPlayerModal: state => {
        return state.displayPlayerModal;
    },
    selectPlayer: state => {
        return state.selectPlayer;
    }
}

export const player = {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
