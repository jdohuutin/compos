# src

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn run serve
```

### Compiles and minifies for production
```
yarn run build
```

### Run your tests
```
yarn run test
```

### Lints and fixes files
```
yarn run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).


### Model data from user

     {
         "name": "rotaldo",
         "picked": false,
         "stats": {
             "attack": 4,
             "defense": 4,
             "stamina": 3,
             "speed": 3
         }
     },
     
     
### Login firebase

- norsyscompos@gmail.com
- norsys69
     