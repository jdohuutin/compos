define binary
$(RM) $1
$(call write,$1,'#!/usr/bin/env sh')
$(call write,$1,'set -e')
$(call write,$1,'exec docker-compose run --rm $$DOCKER_OPTIONS $2 $3 $$@')
chmod u+x $1
endef

define write
echo $2 >> $1
endef

%/.:
	mkdir -p $@

PHONY: install
install: ./bin/. ./bin/vue ./bin/yarn ./bin/npm

PHONY: build
build:
	docker-compose build

./bin/vue:
	$(call binary,$@,node,vue)

./bin/yarn:
	$(call binary,$@,node,yarn)

./bin/npm:
	$(call binary,$@,node,npm)

PHONY: start
start:
	docker-compose up -d

PHONY: stop
stop:
	docker-compose down --remove-orphans

PHONY: restart
restart: stop start
